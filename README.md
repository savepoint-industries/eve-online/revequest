# rEVEquest

rEVEquest is an API pulling Gem for EVE Online related APIs. Planned functionality will include both 3rd Party as well as ESI API calls as wrappers with utility methods for working with the individual API results.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'revequest'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install revequest

## Usage

### Revequest::Appraisal
For Single-Item Appraisals via EvePraisal, the following can be used:
```ruby
    @appraisal = Revequest::Appraisal.new
    @appraisal.generate($system_name, $item)
    results = @appraisal.body.dig("layer1", "layer2" ...)
```
EvePraisal also supports multi-item appraisals via Arrays in the result:
```ruby
    @appraisal = Revequest::Appraisal.new
    @appraisal.generate($system_name, "items, comma separated")
    for each in @appraisal.body.dig("items")
        result[n] = @appraisal.body.dig("items","n","layer2)
    end
```
It's worth mentioning that for the multi-item appraisals, the current system does not support quantity in the request field. The single-item request can generate appraisals with specific quantities however.


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Please contact OldSpiceland @ TavernSavePoint dot Com if you wish to contribute to the project.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Revequest project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/revequest/blob/master/CODE_OF_CONDUCT.md).
