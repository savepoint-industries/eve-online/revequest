require_relative "revequest/version"
require_relative "revequest/appraisal"

module Revequest
  class Error < StandardError; end
  APPRAISAL_URI = "https://evepraisal.com/appraisal.json?"
end
