require 'json'
require 'net/http'

module Revequest
  # Uses 3rd Party EvePraisal API to return pricing data
  class Appraisal

    # Processes and creates a new EvePraisal API Call and returns the response
    # body as a Hash
    def generate (mrkt_sys, rta)
      #TODO: Need to clean up this if possible. Functional but not Elegant
      # Creates an instance of the appraisal URI for modification and query
      uri = URI(Revequest::APPRAISAL_URI)
      params = {:market => mrkt_sys,:raw_textarea => rta, :persist => 'no'}
      uri.query = URI.encode_www_form(params)

      # Uses post to submit appraisal query and returns result as JSON string
      result = Net::HTTP.post_form(uri, 'q' => 'ruby', 'max' => 50)

      # Parses the result into usable format ending with JSON converted to Hash
      result['Set-Cookie']
      result.get_fields('set-cookie')
      result.to_hash['set-cookie']
      @body = JSON.parse(result.body)

      # Unwraps the first layer of the Hash'd object to make it more usable
      @body = @body["appraisal"]
    end

    # Declares an attribute reader backed by identically named instance
    # variable
    def body
      @body
    end

    public :body, :generate
  end
end
